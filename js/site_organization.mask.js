/**
 * @file
 * Misc JQuery scripts in this file
 */
(function($, Drupal, drupalSettings) {
  "use strict";
  $(document).ready(function() {
    if ($.isFunction($.fn.mask)) {
      $(".form-number__inn").mask("000000000000", { placeholder: "667905667862" });
      $(".form-number__inn_legal").mask("0000000000", { placeholder: "5579056678" });
      $(".form-number__ogrnip").mask("000000000000000", { placeholder: "314220119000137" });
      $(".form-number__ogrn").mask("0000000000000", { placeholder: "5077746887312" });
      $(".form-number__kpp").mask("000000000", { placeholder: "773301001" });
    }
  });
})(jQuery, Drupal, drupalSettings);
