<?php

namespace Drupal\site_organization\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining contractors and user groups entities.
 *
 * @ingroup site_organization
 */
interface SiteOrganizationInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  /**
   * Gets the contractors and user groups name.
   *
   * @return string
   *   Name of the contractors and user groups.
   */
  public function getName();

  /**
   * Sets the contractors and user groups name.
   *
   * @param string $name
   *   The contractors and user groups name.
   *
   * @return \Drupal\site_organization\Entity\SiteOrganizationInterface
   *   The called contractors and user groups entity.
   */
  public function setName($name);

  /**
   * Gets the contractors and user groups creation timestamp.
   *
   * @return int
   *   Creation timestamp of the contractors and user groups.
   */
  public function getCreatedTime();

  /**
   * Sets the contractors and user groups creation timestamp.
   *
   * @param int $timestamp
   *   The contractors and user groups creation timestamp.
   *
   * @return \Drupal\site_organization\Entity\SiteOrganizationInterface
   *   The called contractors and user groups entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the contractors and user groups published status indicator.
   *
   * Unpublished contractors and user groups are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the contractors and user groups is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a contractors and user groups.
   *
   * @param bool $published
   *   TRUE to set this contractors and user groups to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\site_organization\Entity\SiteOrganizationInterface
   *   The called contractors and user groups entity.
   */
  public function setPublished($published);

}
