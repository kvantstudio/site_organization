<?php

namespace Drupal\site_organization\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for contractors and user groups entities.
 */
class SiteOrganizationViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    return $data;
  }

}
