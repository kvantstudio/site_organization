<?php

namespace Drupal\site_organization\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the organization type entity.
 *
 * @ConfigEntityType(
 *   id = "site_organization_type",
 *   label = @Translation("Type of organization"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\site_organization\SiteOrganizationTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\site_organization\Form\SiteOrganizationTypeForm",
 *       "edit" = "Drupal\site_organization\Form\SiteOrganizationTypeForm",
 *       "delete" = "Drupal\site_organization\Form\SiteOrganizationTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\site_organization\SiteOrganizationTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "site_organization_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "site_organization",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/site_organization_type/{site_organization_type}",
 *     "add-form" = "/admin/structure/site_organization_type/add",
 *     "edit-form" = "/admin/structure/site_organization_type/{site_organization_type}/edit",
 *     "delete-form" = "/admin/structure/site_organization_type/{site_organization_type}/delete",
 *     "collection" = "/admin/structure/organizations-types"
 *   }
 * )
 */
class SiteOrganizationType extends ConfigEntityBundleBase implements SiteOrganizationTypeInterface {

  /**
   * The contractors and user groups type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The contractors and user groups type label.
   *
   * @var string
   */
  protected $label;

}
