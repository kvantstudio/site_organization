<?php

namespace Drupal\site_organization\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining contractors and user groups type entities.
 */
interface SiteOrganizationTypeInterface extends ConfigEntityInterface {

}
