<?php

namespace Drupal\site_organization\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\user\UserInterface;

/**
 * Defines the organization entity.
 *
 * @ingroup site_organization
 *
 * @ContentEntityType(
 *   id = "site_organization",
 *   label = @Translation("Organization"),
 *   bundle_label = @Translation("Сontractor or users group"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\site_organization\SiteOrganizationListBuilder",
 *     "views_data" = "Drupal\site_organization\Entity\SiteOrganizationViewsData",
 *     "translation" = "Drupal\site_organization\SiteOrganizationTranslationHandler",
 *     "form" = {
 *       "default" = "Drupal\site_organization\Form\SiteOrganizationForm",
 *       "add" = "Drupal\site_organization\Form\SiteOrganizationForm",
 *       "edit" = "Drupal\site_organization\Form\SiteOrganizationForm",
 *       "delete" = "Drupal\site_organization\Form\SiteOrganizationDeleteForm",
 *     },
 *     "access" = "Drupal\site_organization\SiteOrganizationAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\site_organization\SiteOrganizationHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "site_organization",
 *   data_table = "site_organization_field_data",
 *   translatable = TRUE,
 *   admin_permission = "administer contractors and user groups",
 *   entity_keys = {
 *     "id" = "id",
 *     "bundle" = "type",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "uid",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *     "checked" = "checked",
 *   },
 *   links = {
 *     "canonical" = "/contractor/{site_organization}",
 *     "add-page" = "/admin/structure/site_organization/add",
 *     "add-form" = "/admin/structure/site_organization/add/{site_organization_type}",
 *     "edit-form" = "/admin/structure/site_organization/{site_organization}/edit",
 *     "delete-form" = "/admin/structure/site_organization/{site_organization}/delete",
 *     "collection" = "/admin/content/organizations",
 *   },
 *   bundle_entity_type = "site_organization_type",
 *   field_ui_base_route = "entity.site_organization_type.edit_form"
 * )
 */
class SiteOrganization extends ContentEntityBase implements SiteOrganizationInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'uid' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('uid')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('uid')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('uid', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('uid', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isChecked() {
    return (bool) $this->getEntityKey('checked');
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', $published ? TRUE : FALSE);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isVirtualAccount() {
    return (bool) $this->get('virtual_account')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setVirtualAccount($group) {
    $this->set('virtual_account', $group ? 1 : 0);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function uuid() {
    return $this->getEntityKey('uuid');
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['pid'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Parent ID'))
      ->setDescription(t('The parent ID of organization.'))
      ->setDefaultValue(0);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Author'))
      ->setDescription(t('The user ID of author.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Display name'))
      ->setTranslatable(TRUE)
      ->setRequired(TRUE)
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['virtual_account'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Virtual account'))
      ->setDescription(t('Select this value if the account is virtual.'))
      ->setDefaultValue(FALSE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => TRUE,
        ],
        'weight' => 10,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Publishing status'))
      ->setDescription(t('A boolean indicating whether the organization is published.'))
      ->setDefaultValue(TRUE);

    $fields['checked'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Сhecked'))
      ->setDescription(t('A boolean indicating whether the organization is сhecked.'))
      ->setDefaultValue(FALSE);

    $fields['default_profile'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Main profile'))
      ->setDescription(t('A boolean indicating whether the organization is default for user.'))
      ->setDefaultValue(FALSE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time was last edited.'));

    return $fields;
  }
}
