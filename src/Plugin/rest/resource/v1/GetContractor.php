<?php

namespace Drupal\site_organization\Plugin\rest\resource\v1;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a resource to get contractor by tin.
 *
 * @RestResource(
 *   id = "site_organization_get_contractor",
 *   label = @Translation("Organizations: get contractor"),
 *   uri_paths = {
 *     "canonical" = "/api/v1/get-contractor",
 *   }
 * )
 */
class GetContractor extends ResourceBase {

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a new GetContractorByTin object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AccountProxyInterface $current_user) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('site_organization'),
      $container->get('current_user')
    );
  }

  /**
   * Responds to GET requests.
   */
  public function get() {
    $response['result'] = NULL;

    $query = \Drupal::request()->query;
    if ($query->has('token') && $query->has('inn')) {
      $token = $query->get('token');
      $inn = (int) $query->get('inn');

      // Загружаем конфигурацию.
      $config = \Drupal::config('site_organization.settings');
      $access_token = $config->get('access_token');

      if ($access_token == $token) {
        $contractor = \Drupal::service('site_organization.database')->loadContractorByINN($inn);
        if ($contractor) {
          $response['status'] = TRUE;
          $response['result']['contractor'] = $contractor;
        } else {
          $response['status'] = FALSE;
        }
      } else {
        $response['status_message'] = "Доступ запрещен. Отправлено сообщение в службу безопасности.";
        $request = [
          'inn' => $inn,
          'access_token' => $access_token,
          'token' => $token,
        ];
        \Drupal::logger('site_organization')->error('Ошибка проверки токена при проверке регистрации: ' . json_encode($request, JSON_UNESCAPED_UNICODE));

        return new ResourceResponse($response, 403);
      }

      return new ModifiedResourceResponse($response);
    } else {
      return new ResourceResponse($response, 400);
    }
  }
}
