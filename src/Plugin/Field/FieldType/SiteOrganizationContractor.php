<?php

namespace Drupal\site_organization\Plugin\Field\FieldType;

use Drupal\Component\Utility\Random;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'site_organization_contractor' field type.
 *
 * @FieldType(
 *   id = "site_organization_contractor",
 *   label = @Translation("Contractor"),
 *   description = @Translation("List of my contractors."),
 *   module = "site_organization",
 *   category = @Translation("Contractors and user groups"),
 *   default_widget = "site_organization_contractor_default_widget",
 *   default_formatter = "site_organization_contractor_default_formatter"
 * )
 */
class SiteOrganizationContractor extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = [];

    $properties['target_id'] = DataDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Contractor ID'))
      ->setSetting('unsigned', TRUE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $schema = [
      'columns' => [
        'target_id' => [
          'type' => 'int',
          'not null' => false,
          'default' => 0,
          'unsigned' => true,
        ],
      ],
      'indexes' => [],
    ];

    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $isEmpty = TRUE;
    if ($this->get('target_id')->getValue()) {
      $isEmpty = FALSE;
    }

    return $isEmpty;
  }
}