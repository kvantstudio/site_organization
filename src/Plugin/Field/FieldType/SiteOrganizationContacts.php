<?php

namespace Drupal\site_organization\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Render\Element\Email;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'site_organization_contacts' field type.
 *
 * @FieldType(
 *   id = "site_organization_contacts",
 *   label = @Translation("Contact information"),
 *   description = @Translation("Contact information of contractors."),
 *   default_widget = "site_organization_contacts_default_widget",
 *   default_formatter = "site_organization_contacts_default_formatter"
 * )
 */
class SiteOrganizationContacts extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = [];

    $properties['uid'] = DataDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('User ID Reference'))
      ->setDescription(new TranslatableMarkup('The ID of the referenced user.'))
      ->setSetting('unsigned', TRUE);

    $properties['type'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Organizational and legal form'));

    $properties['last_name'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Last name'));

    $properties['first_name'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('First name'));

    $properties['patronymic'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Patronymic'));

    $properties['post'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Post'));

    $properties['inn'] = DataDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('TIN'))
      ->setSetting('unsigned', TRUE);

    $properties['ogrnip'] = DataDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('PSRNSP'))
      ->setSetting('unsigned', TRUE);

    $properties['ogrn'] = DataDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('PSRN'))
      ->setSetting('unsigned', TRUE);

    $properties['kpp'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('IEC'));

    $properties['telephone'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Telephone'));

    $properties['email'] = DataDefinition::create('email')
      ->setLabel(new TranslatableMarkup('Email'));

    $properties['site'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Organization website'));

    $properties['legal_address'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Legal address'));

    $properties['location_address'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Location address'));

    $properties['postal_address'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Postal address'));

    $properties['description'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Description'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $schema = [
      'columns' => [
        'uid' => [
          'type' => 'int',
          'not null' => false,
          'default' => 0,
          'unsigned' => true,
        ],
        'type' => [
          'type' => 'varchar',
          'length' => 255,
          'not null' => false,
          'default' => '',
        ],
        'last_name' => [
          'type' => 'varchar',
          'length' => 255,
          'not null' => false,
          'default' => '',
        ],
        'first_name' => [
          'type' => 'varchar',
          'length' => 255,
          'not null' => false,
          'default' => '',
        ],
        'patronymic' => [
          'type' => 'varchar',
          'length' => 255,
          'not null' => false,
          'default' => '',
        ],
        'post' => [
          'type' => 'varchar',
          'length' => 255,
          'not null' => false,
          'default' => '',
        ],
        'inn' => [
          'type' => 'int',
          'size' => 'big',
          'not null' => false,
          'default' => 0,
          'unsigned' => true,
        ],
        'ogrnip' => [
          'type' => 'int',
          'size' => 'big',
          'not null' => false,
          'default' => 0,
          'unsigned' => true,
        ],
        'ogrn' => [
          'type' => 'int',
          'size' => 'big',
          'not null' => false,
          'default' => 0,
          'unsigned' => true,
        ],
        'kpp' => [
          'type' => 'int',
          'not null' => false,
          'default' => 0,
          'unsigned' => true,
        ],
        'telephone' => [
          'type' => 'varchar',
          'length' => 256,
          'not null' => false,
          'default' => '',
        ],
        'email' => [
          'type' => 'varchar',
          'length' => Email::EMAIL_MAX_LENGTH,
          'not null' => false,
          'default' => '',
        ],
        'site' => [
          'type' => 'varchar',
          'length' => 255,
          'not null' => false,
          'default' => '',
        ],
        'legal_address' => [
          'type' => 'text',
          'size' => 'big',
          'not null' => false,
        ],
        'location_address' => [
          'type' => 'text',
          'size' => 'big',
          'not null' => false,
        ],
        'postal_address' => [
          'type' => 'text',
          'size' => 'big',
          'not null' => false,
        ],
        'description' => [
          'type' => 'text',
          'size' => 'big',
          'not null' => false,
        ],
      ],
      'indexes' => [],
    ];

    return $schema;
  }

  /**
   * Формирует массив полей для администратора, в котором ключи - идентификаторы полей, значение - переведенное название поля.
   *
   * @return array list available fields.
   */
  public static function getFieldsListAdmin() {
    $fields = [
      'type' => 'type (' . new TranslatableMarkup('Organizational and legal form') . ')',
      'last_name' => 'last_name (' . new TranslatableMarkup('Last name') . ')',
      'first_name' => 'first_name (' . new TranslatableMarkup('First name') . ')',
      'patronymic' => 'patronymic (' . new TranslatableMarkup('Patronymic') . ')',
      'post' => 'post (' . new TranslatableMarkup('Post') . ')',
      'inn' => 'inn (' . new TranslatableMarkup('TIN') . ')',
      'ogrnip' => 'ogrnip (' . new TranslatableMarkup('PSRNSP') . ')',
      'ogrn' => 'ogrn (' . new TranslatableMarkup('PSRN') . ')',
      'kpp' => 'kpp (' . new TranslatableMarkup('IEC') . ')',
      'telephone' => 'telephone (' . new TranslatableMarkup('Telephone') . ')',
      'email' => 'email (' . new TranslatableMarkup('Email') . ')',
      'site' => 'site (' . new TranslatableMarkup('Organization website') . ')',
      'legal_address' => 'legal_address (' . new TranslatableMarkup('Legal address') . ')',
      'location_address' => 'location_address (' . new TranslatableMarkup('Location address') . ')',
      'postal_address' => 'postal_address (' . new TranslatableMarkup('Postal address') . ')',
    ];

    return $fields;
  }

}
