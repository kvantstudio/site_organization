<?php

namespace Drupal\site_organization\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\site_organization\Plugin\Field\FieldType\SiteOrganizationContacts;

/**
 * Plugin implementation of the 'site_organization_contacts_default_widget' widget.
 *
 * @FieldWidget(
 *   id = "site_organization_contacts_default_widget",
 *   label = @Translation("Default"),
 *   field_types = {
 *     "site_organization_contacts"
 *   }
 * )
 */
class SiteOrganizationContactsDefaultWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $options = parent::defaultSettings();

    $fields = SiteOrganizationContacts::getFieldsListAdmin();
    $options['fields_contacts'] = $fields;

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {

    $fields = SiteOrganizationContacts::getFieldsListAdmin();

    $element['fields_contacts'] = [
      '#type' => 'checkboxes',
      '#required' => true,
      '#options' => $fields,
      '#title' => t('Select available fields'),
      '#default_value' => $this->getSetting('fields_contacts'),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $fields_contacts = array_diff($this->getSetting('fields_contacts'), array(0, NULL));
    $summary[] = t('Available fields: @fields.', array('@fields' => count($fields_contacts)));

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = [];

    $fields_contacts = array_diff($this->getSetting('fields_contacts'), array(0, NULL));

    $element['type'] = array(
      '#type' => 'select',
      '#title' => $this->t('Organizational and legal form'),
      '#options' => $this->getTypes(),
      '#default_value' => isset($items[$delta]->type) ? $items[$delta]->type : 0,
      '#access' => in_array('type', $fields_contacts) ? TRUE : FALSE,
    );

    if (!in_array('ogrn', $fields_contacts)) {
      $element['last_name'] = array(
        '#type' => 'textfield',
        '#title' => $this->t('Last name'),
        '#default_value' => isset($items[$delta]->last_name) ? $items[$delta]->last_name : NULL,
        '#empty_value' => '',
        '#maxlength' => 255,
        '#access' => in_array('last_name', $fields_contacts) ? TRUE : FALSE,
      );

      $element['first_name'] = array(
        '#type' => 'textfield',
        '#title' => $this->t('First name'),
        '#default_value' => isset($items[$delta]->first_name) ? $items[$delta]->first_name : NULL,
        '#empty_value' => '',
        '#maxlength' => 255,
        '#access' => in_array('first_name', $fields_contacts) ? TRUE : FALSE,
      );

      $element['patronymic'] = array(
        '#type' => 'textfield',
        '#title' => $this->t('Patronymic'),
        '#default_value' => isset($items[$delta]->patronymic) ? $items[$delta]->patronymic : NULL,
        '#empty_value' => '',
        '#maxlength' => 255,
        '#access' => in_array('patronymic', $fields_contacts) ? TRUE : FALSE,
      );
    }

    $element['telephone'] = array(
      '#type' => 'tel',
      '#title' => $this->t('Telephone'),
      '#default_value' => isset($items[$delta]->telephone) ? $items[$delta]->telephone : NULL,
      '#empty_value' => '',
      '#maxlength' => 256,
      '#size' => 60,
      '#access' => in_array('telephone', $fields_contacts) ? TRUE : FALSE,
    );

    $element['email'] = array(
      '#type' => 'email',
      '#title' => $this->t('Email'),
      '#default_value' => isset($items[$delta]->email) ? $items[$delta]->email : NULL,
      '#empty_value' => '',
      '#maxlength' => 255,
      '#access' => in_array('email', $fields_contacts) ? TRUE : FALSE,
    );

    $element['site'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Organization website'),
      '#default_value' => isset($items[$delta]->site) ? $items[$delta]->site : NULL,
      '#empty_value' => '',
      '#maxlength' => 255,
      '#access' => in_array('site', $fields_contacts) ? TRUE : FALSE,
    );

    $element['legal_address'] = array(
      '#type' => 'textarea',
      '#title' => $this->t('Legal address'),
      '#default_value' => isset($items[$delta]->legal_address) ? $items[$delta]->legal_address : NULL,
      '#empty_value' => '',
      '#maxlength' => 1024,
      '#access' => in_array('legal_address', $fields_contacts) ? TRUE : FALSE,
    );

    $element['location_address'] = array(
      '#type' => 'textarea',
      '#title' => $this->t('Location address'),
      '#default_value' => isset($items[$delta]->location_address) ? $items[$delta]->location_address : NULL,
      '#empty_value' => '',
      '#maxlength' => 1024,
      '#access' => in_array('location_address', $fields_contacts) ? TRUE : FALSE,
    );

    $element['postal_address'] = array(
      '#type' => 'textarea',
      '#title' => $this->t('Postal address'),
      '#default_value' => isset($items[$delta]->postal_address) ? $items[$delta]->postal_address : NULL,
      '#empty_value' => '',
      '#maxlength' => 1024,
      '#access' => in_array('postal_address', $fields_contacts) ? TRUE : FALSE,
    );

    if (in_array('ogrn', $fields_contacts)) {

      $element['person_contact_prefix'] = array(
        '#markup' => '<div class="site-organization-form__container"><h2>' . $this->t('Person contact') . '</h2>',
      );

      $element['post'] = array(
        '#type' => 'textfield',
        '#title' => $this->t('Post'),
        '#default_value' => isset($items[$delta]->post) ? $items[$delta]->post : NULL,
        '#empty_value' => '',
        '#maxlength' => 255,
        '#access' => in_array('post', $fields_contacts) ? TRUE : FALSE,
      );

      $element['last_name'] = array(
        '#type' => 'textfield',
        '#title' => $this->t('Last name'),
        '#default_value' => isset($items[$delta]->last_name) ? $items[$delta]->last_name : NULL,
        '#empty_value' => '',
        '#maxlength' => 255,
        '#access' => in_array('last_name', $fields_contacts) ? TRUE : FALSE,
      );

      $element['first_name'] = array(
        '#type' => 'textfield',
        '#title' => $this->t('First name'),
        '#default_value' => isset($items[$delta]->first_name) ? $items[$delta]->first_name : NULL,
        '#empty_value' => '',
        '#maxlength' => 255,
        '#access' => in_array('first_name', $fields_contacts) ? TRUE : FALSE,
      );

      $element['patronymic'] = array(
        '#type' => 'textfield',
        '#title' => $this->t('Patronymic'),
        '#default_value' => isset($items[$delta]->patronymic) ? $items[$delta]->patronymic : NULL,
        '#empty_value' => '',
        '#maxlength' => 255,
        '#access' => in_array('patronymic', $fields_contacts) ? TRUE : FALSE,
      );

      $element['person_contact_suffix'] = array(
        '#markup' => '</div>',
      );

    }

    $element['requisites_prefix'] = array(
      '#markup' => '<div class="site-organization-form__container"><h2>' . $this->t('Requisites') . '</h2>',
    );

    // Определяем класс для маски ИНН.
    $class = 'form-number__inn';
    $size = 12;
    if (in_array('ogrn', $fields_contacts)) {
      $class = 'form-number__inn_legal';
      $size = 10;
    }
    $element['inn'] = array(
      '#type' => 'number',
      '#title' => $this->t('TIN'),
      '#default_value' => isset($items[$delta]->inn) && $items[$delta]->inn ? $items[$delta]->inn : NULL,
      '#maxlength' => $size,
      '#size' => $size,
      '#access' => in_array('inn', $fields_contacts) ? TRUE : FALSE,
      '#required' => in_array('ogrnip', $fields_contacts) || in_array('ogrn', $fields_contacts) ? TRUE : FALSE,
      '#element_validate' => [
        [$this, 'validate_inn'],
      ],
      '#attributes' => array('placeholder' => '', 'class' => ['form-number', $class]),
    );

    $element['ogrnip'] = array(
      '#type' => 'number',
      '#title' => $this->t('PSRNSP'),
      '#default_value' => isset($items[$delta]->ogrnip) && $items[$delta]->ogrnip ? $items[$delta]->ogrnip : NULL,
      '#maxlength' => 15,
      '#size' => 15,
      '#access' => in_array('ogrnip', $fields_contacts) ? TRUE : FALSE,
      '#element_validate' => [
        [$this, 'validate_ogrnip'],
      ],
      '#attributes' => array('placeholder' => '', 'class' => ['form-number', 'form-number__ogrnip']),
    );

    $element['ogrn'] = array(
      '#type' => 'number',
      '#title' => $this->t('PSRN'),
      '#default_value' => isset($items[$delta]->ogrn) && $items[$delta]->ogrn ? $items[$delta]->ogrn : NULL,
      '#maxlength' => 13,
      '#size' => 13,
      '#access' => in_array('ogrn', $fields_contacts) ? TRUE : FALSE,
      '#element_validate' => [
        [$this, 'validate_ogrn'],
      ],
      '#attributes' => array('placeholder' => '', 'class' => ['form-number', 'form-number__ogrn']),
    );

    $element['kpp'] = array(
      '#type' => 'number',
      '#title' => $this->t('IEC'),
      '#default_value' => isset($items[$delta]->kpp) && $items[$delta]->kpp ? $items[$delta]->kpp : NULL,
      '#maxlength' => 9,
      '#size' => 9,
      '#access' => in_array('kpp', $fields_contacts) ? TRUE : FALSE,
      '#element_validate' => [
        [$this, 'validate_kpp'],
      ],
      '#attributes' => array('placeholder' => '', 'class' => ['form-number', 'form-number__kpp']),
    );

    $element['requisites_suffix'] = array(
      '#markup' => '</div>',
    );

    return $element;
  }

  /**
   * Validate inn field.
   */
  public function validate_inn($element, FormStateInterface $form_state) {
    $fields_contacts = array_diff($this->getSetting('fields_contacts'), array(0, NULL));

    $value = $element['#value'];

    $size = $element['#size'];

    if ($value > 0 && strlen($value) != $size) {
      $form_state->setError($element, $this->t("TIN must be a @value digit.", ['@value' => $size]));
      return;
    }

    if (in_array('ogrnip', $fields_contacts) || in_array('ogrn', $fields_contacts)) {
      $default_value = $element['#default_value'];
      // Вновь создаваемый контрагент.
      if (!$default_value) {
        $contractor = \Drupal::service('site_organization.database')->loadContractorByINN($value);
        if ($contractor) {
          $form_state->setError($element, $this->t("The contractor with TIN @value is already registered.", ['@value' => $value]));
          return;
        }
      }

      // При редактировании контагента.
      if ($default_value && $default_value != $value) {
        $contractor = \Drupal::service('site_organization.database')->loadContractorByINN($value);
        if ($contractor) {
          $form_state->setError($element, $this->t("The contractor with TIN @value is already registered.", ['@value' => $value]));
          return;
        }
      }
    }

    if (strlen($value) == 0) {
      $form_state->setValueForElement($element, 0);
      return;
    }
  }

  /**
   * Validate ogrnip field.
   */
  public function validate_ogrnip($element, FormStateInterface $form_state) {
    $value = $element['#value'];
    if (strlen($value) == 0) {
      $form_state->setValueForElement($element, 0);
      return;
    }
  }

  /**
   * Validate ogrn field.
   */
  public function validate_ogrn($element, FormStateInterface $form_state) {
    $value = $element['#value'];
    if (strlen($value) == 0) {
      $form_state->setValueForElement($element, 0);
      return;
    }
  }

  /**
   * Validate kpp field.
   */
  public function validate_kpp($element, FormStateInterface $form_state) {
    $value = $element['#value'];
    if (strlen($value) == 0) {
      $form_state->setValueForElement($element, 0);
      return;
    }
  }

  /**
   * Формирует варианты организационно правовых форм.
   */
  private function getTypes() {
    return [
      '' => 'Не выбрано',
      'полное товарищество' => 'полное товарищество',
      'товарищество на вере' => 'товарищество на вере',
      'ООО' => 'общество с ограниченной ответственностью',
      'АО' => 'акционерное общество',
      'АНПО' => 'автономная некоммерческая просветительская организация',
      'НАО' => 'непубличное акционерное общество',
      'ПАО' => 'публичное акционерное общество',
      'ЗАО' => 'закрытое акционерное общество',
      'ОАО' => 'открытое акционерное общество',
    ];
  }

}
