<?php

namespace Drupal\site_organization\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'site_organization_contractor_default_widget' widget.
 *
 * @FieldWidget(
 *   id = "site_organization_contractor_default_widget",
 *   label = @Translation("Default"),
 *   field_types = {
 *     "site_organization_contractor"
 *   }
 * )
 */
class SiteOrganizationContractorDefaultWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = [];

    $contractors = $this->getContractors();
    $element['target_id'] = array(
      '#type' => 'select',
      '#title' => $this->t('Contractor'),
      '#options' => $contractors,
      '#default_value' => isset($items[$delta]->target_id) ? $items[$delta]->target_id : 0,
      '#access' => count($contractors) > 1 ? TRUE : FALSE,
    );

    return $element;
  }

  /**
   * Формирует список контрагентов доступных текущему пользователю.
   */
  private function getContractors() {
    $data = ['0' => 'Не выбрано'];

    // Список контрагентов.
    $data += \Drupal::service('site_organization.database')->getContractorsForUser(\Drupal::currentUser()->id());

    return $data;
  }

}
