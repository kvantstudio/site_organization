<?php

namespace Drupal\site_organization;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Access controller for the contractors and user groups entity.
 *
 * @see \Drupal\site_organization\Entity\SiteOrganization.
 */
class SiteOrganizationAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\site_organization\Entity\SiteOrganizationInterface $entity */
    switch ($operation) {
    case 'view':
      if (!$entity->isPublished() && !$entity->isVirtualAccount()) {
        return AccessResult::allowedIfHasPermission($account, 'site organization allow view unpublished contractors');
      }

      if ($entity->isPublished() && !$entity->isVirtualAccount()) {
        if ($entity->getOwnerId() != $account->id()) {
          return AccessResult::allowedIfHasPermission($account, 'site organization allow view published contractors');
        }
        if ($entity->getOwnerId() == $account->id()) {
          return AccessResult::allowedIfHasPermission($account, 'site organization allow view own contractors');
        }
      }

      if ($entity->isVirtualAccount()) {
        return AccessResult::allowedIfHasPermission($account, 'administer contractors and user groups');
      }

    case 'update':

      if ($entity->isVirtualAccount()) {
        return AccessResult::allowedIfHasPermission($account, 'administer contractors and user groups');
      } else {
        if ($entity->getOwnerId() != $account->id()) {
          return AccessResult::allowedIfHasPermission($account, 'site organization allow edit contractors');
        }
        if ($entity->getOwnerId() == $account->id()) {
          return AccessResult::allowedIfHasPermission($account, 'site organization allow edit own contractors');
        }
      }

    case 'delete':
      return AccessResult::allowedIfHasPermission($account, 'site organization allow delete contractors');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'site organization allow add contractors');
  }
}
