<?php

namespace Drupal\site_organization\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\site_organization\Entity\SiteOrganization;
use Drupal\site_organization\Entity\SiteOrganizationType;

class SiteOrganizationUserFormController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function add(SiteOrganizationType $entity_type) {
    if ($entity_type->id()) {
      $site_organization = \Drupal::entityTypeManager()->getStorage('site_organization')->create([
        'type' => $entity_type->id(),
        'virtual_account' => $entity_type->id() == 'type_1' ? 1 : 0,
      ]);

      $form_mode = 'default';

      if ($site_organization instanceof SiteOrganization) {
        $data = \Drupal::service('entity.form_builder')->getForm($site_organization, $form_mode);
        $data['#attached']['library'][] = 'site_organization/administer';

        return $data;
      }
    }

    $build = array(
      '#type' => 'markup',
      '#markup' => 'Error on the page!',
    );
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function edit(SiteOrganization $site_organization) {
    $form_mode = 'user';
    $data = \Drupal::service('entity.form_builder')->getForm($site_organization, $form_mode);
    $data['#attached']['library'][] = 'site_organization/administer';

    return $data;

    $build = array(
      '#type' => 'markup',
      '#markup' => 'Error on the page!',
    );
    return $build;
  }

  /**
   * The _title_callback for the site_organization.add route.
   *
   * @param \Drupal\site_organization\SiteOrganizationType $entity_type
   *   The current site_organization_type.
   *
   * @return string
   *   The page title.
   */
  public function getAddTitle(SiteOrganizationType $entity_type) {
    return $this->t('You create contractor «@name»', array('@name' => $entity_type->label()));
  }

  /**
   * The _title_callback for the site_organization.update route.
   *
   * @param \Drupal\site_organization\SiteOrganization $site_organization
   *   The current site_organization.
   *
   * @return string
   *   The page title.
   */
  public function getTitle($site_organization) {
    return $this->t('You are editing «@name»', array('@name' => $site_organization->getName()));
  }
}
