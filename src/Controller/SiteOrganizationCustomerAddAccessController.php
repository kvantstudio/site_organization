<?php

namespace Drupal\site_organization\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;
use Drupal\site_organization\Entity\SiteOrganizationType;

/**
 * Builds an example page.
 */
class SiteOrganizationCustomerAddAccessController {

  /**
   * Checks access for a specific request.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   */
  public function access(AccountInterface $account, SiteOrganizationType $entity_type) {
    $type = $entity_type->id();
    if ($type == 'type_1') {
      return AccessResult::allowedIf($account->hasPermission('site organization allow add user groups'));
    }
    return AccessResult::allowedIf($account->hasPermission('site organization allow add contractors'));
  }
}
