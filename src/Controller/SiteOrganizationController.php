<?php

namespace Drupal\site_organization\Controller;

use Drupal\Core\Controller\ControllerBase;

class SiteOrganizationController extends ControllerBase {

  /**
   * Главная страница списка организаций.
   */
  public function content() {
    $site_organization_content = array(
      '#theme' => 'site_organization_content',
    );

    return array(
      '#type' => 'container',
      $site_organization_content,
    );
  }
}