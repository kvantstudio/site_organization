<?php

namespace Drupal\site_organization\Controller;

use Drupal\Component\Utility\Html;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\site_organization\Entity\SiteOrganization;
use Symfony\Component\DependencyInjection\ContainerInterface;

class SiteOrganizationDatabaseController extends ControllerBase {

  /**
   * The database connection object.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Constructs a new DblogClearLogForm.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   */
  public function __construct(Connection $connection) {
    $this->connection = $connection;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database')
    );
  }

  /**
   * Назначает пользователю организацию.
   */
  public function setOrganizationForUser($id, $uid, $access = TRUE) {
    $fields = array(
      'id' => (int) $id,
      'uid' => (int) $uid,
      'access' => (bool) $access,
    );
    $query = $this->connection->insert('site_organization_users');
    $query->fields($fields);
    $query->execute();
  }

  /**
   * Возвращает объект контрагента по ИНН.
   */
  public function loadContractorByINN(int $inn) {
    $query = $this->connection->select('site_organization__field_contact_information', 'n');
    $query->fields('n', ['entity_id']);
    $query->condition('n.field_contact_information_inn', $inn);
    $id = $query->execute()->fetchField();

    $data = [];
    if ($id) {
      $contractor = SiteOrganization::load($id);
      $data = [
        'uuid' => $contractor->uuid(),
        'name' => $contractor->getName(),
        'checked' => $contractor->isChecked(),
        'type' => $contractor->field_contact_information->type,
        'iin' => $contractor->field_contact_information->inn,
        'ogrnip' => $contractor->field_contact_information->ogrnip,
        'ogrn' => $contractor->field_contact_information->ogrn,
        'kpp' => $contractor->field_contact_information->kpp,
        'telephone' => $contractor->field_contact_information->telephone,
        'email' => $contractor->field_contact_information->email,
        'legal_address' => $contractor->field_contact_information->legal_address,
      ];
    }

    return $data;
  }

  /**
   * Загружает перечень контрагентов для учётной записи.
   */
  public function getContractorsForUser($uid) {
    $account = \Drupal\user\Entity\User::load($uid);
    $contractors = array();
    $query = $this->connection->select('site_organization_field_data', 'n');
    $query->fields('n', array('id', 'name'));
    if (!$account->hasPermission('site organization allow view published contractors')) {
      if ($account->hasPermission('site organization allow view own contractors')) {
        $query->condition('n.uid', $account->id());
      } else {
        return $contractors;
      }
    }
    $query->condition('n.status', 1);
    $query->orderBy('n.name', 'ASC');
    $result = $query->execute();

    foreach ($result as $row) {
      $contractors[$row->id] = Html::escape($row->name);
    }

    return $contractors;
  }

  /**
   * Загружает перечень контрагентов для учётной записи по типу.
   */
  public function getContractorsForUserByType($uid, $type) {
    $account = \Drupal\user\Entity\User::load($uid);
    $contractors = array();
    $query = $this->connection->select('site_organization_field_data', 'n');
    $query->fields('n', array('id', 'name'));
    if ($account->hasPermission('site organization allow view own contractors')) {
      $query->condition('n.uid', $account->id());
    } else {
      return $contractors;
    }
    $query->condition('n.status', 1);
    $query->condition('n.type', $type);
    $query->orderBy('n.name', 'ASC');
    $result = $query->execute();

    foreach ($result as $row) {
      $contractors[$row->id] = Html::escape($row->name);
    }

    return $contractors;
  }

  /**
   * Загружает идентификатор контрагента по умолчанию для пользователя.
   */
  public function getDefaultContractorForUser($uid) {
    $query = $this->connection->select('site_organization_field_data', 'n');
    $query->fields('n', array('id'));
    $query->condition('n.default_profile', 1);
    $query->condition('n.uid', $uid);
    return (int) $query->execute()->fetchField();
  }
}
