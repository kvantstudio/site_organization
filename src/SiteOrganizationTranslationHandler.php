<?php

namespace Drupal\site_organization;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for site_organization.
 */
class SiteOrganizationTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.

}
