<?php

namespace Drupal\site_organization\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting contractors and user groups entities.
 *
 * @ingroup site_organization
 */
class SiteOrganizationDeleteForm extends ContentEntityDeleteForm {

}
