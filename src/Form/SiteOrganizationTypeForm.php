<?php

namespace Drupal\site_organization\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SiteOrganizationTypeForm.
 */
class SiteOrganizationTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $site_organization_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $site_organization_type->label(),
      '#description' => $this->t("Label type of the contractor or users group."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $site_organization_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\site_organization\Entity\SiteOrganizationType::load',
      ],
      '#disabled' => !$site_organization_type->isNew(),
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $site_organization_type = $this->entity;
    $status = $site_organization_type->save();

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created a card «%label».', [
          '%label' => $site_organization_type->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved a card «%label».', [
          '%label' => $site_organization_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($site_organization_type->toUrl('collection'));
  }

}
