<?php

namespace Drupal\site_organization\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Edit form for Organization entity.
 *
 * @ingroup site_organization
 */
class SiteOrganizationForm extends ContentEntityForm {

  /**
   * Constructs a ContentEntityForm object.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   */
  public function __construct(EntityRepositoryInterface $entity_repository, EntityTypeBundleInfoInterface $entity_type_bundle_info = NULL, TimeInterface $time = NULL) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->getEntity();
    $result = $entity->save();

    $edit_link = $entity->toLink($this->t('Edit'), 'edit-form')->toString();
    $view_link = $entity->toLink()->toString();
    switch ($result) {
      case SAVED_NEW:
        $this->messenger()->addStatus($this->t('Created a card «%label».', ['%label' => $view_link]));
        $this->logger('attribute')->notice('Created a card «%label».', ['%label' => $entity->label(), 'link' => $edit_link]);
        break;

      case SAVED_UPDATED:
        $this->messenger()->addStatus($this->t('Saved a card «%label».', ['%label' => $view_link]));
        $this->logger('attribute')->notice('Saved a card «%label».', ['%label' => $entity->label(), 'link' => $edit_link]);
        break;
    }

    $form_state->setRedirect('entity.site_organization.canonical', ['site_organization' => $entity->id()]);
  }

}
