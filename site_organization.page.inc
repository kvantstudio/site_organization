<?php

/**
 * @file
 * Contains site_organization.page.inc.
 *
 * Page callback for contractors and user groups entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for contractors and user groups templates.
 *
 * Default template: site-organization.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_site_organization(array &$variables) {
  // Fetch SiteOrganization Entity Object.
  $site_organization = $variables['elements']['#site_organization'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}

/**
 * Prepares variables for contractors and user groups templates.
 *
 * Default template: site-organization-managment.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_site_organization_managment(array &$variables) {
  $uid = \Drupal::currentUser()->id();

  // Перечень физических лиц.
  $variables['contractors_type_2'] = \Drupal::service('site_organization.database')->getContractorsForUserByType($uid, 'type_2');

  // Перечень индивидуальных предпринимателей.
  $variables['contractors_type_3'] = \Drupal::service('site_organization.database')->getContractorsForUserByType($uid, 'type_3');

  // Перечень юридических лиц.
  $variables['contractors_type_4'] = \Drupal::service('site_organization.database')->getContractorsForUserByType($uid, 'type_4');
}
